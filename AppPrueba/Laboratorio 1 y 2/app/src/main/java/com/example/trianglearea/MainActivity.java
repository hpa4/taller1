package com.example.trianglearea;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
public class MainActivity extends AppCompatActivity {
    EditText etbase, etaltura;
    Button button;
    ImageView img1;
    TextView tv4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etbase = findViewById(R.id.etbase);
        etaltura = findViewById(R.id.etaltura);
        img1 = findViewById(R.id.img1);
        button =findViewById(R.id.button);
        tv4 = findViewById(R.id.tv4);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float base = Float.parseFloat(etbase.getText().toString());
                float alt = Float.parseFloat(etaltura.getText().toString());
                float area = base*alt/2;
                String r = String.valueOf(area);

                tv4.setText("area del triangulo: " +area);
            }
        });
    }
}
